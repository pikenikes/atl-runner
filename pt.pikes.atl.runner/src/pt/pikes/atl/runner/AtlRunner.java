package pt.pikes.atl.runner;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.m2m.atl.common.ATLLogger;
import org.eclipse.m2m.atl.emftvm.ExecEnv;
import org.eclipse.m2m.atl.emftvm.Model;
import org.eclipse.m2m.atl.emftvm.util.TimingData;

import jline.TerminalFactory;
import pt.pikes.atl.runner.configuration.AtlConfiguration;
import pt.pikes.atl.runner.configuration.AtlConfigurationLoader;
import pt.pikes.atl.runner.configuration.AtlModelDefinition;

public class AtlRunner {
	public static void main(String[] args) throws IOException {
		Options options = new Options();
		configureOptions(options);
		GnuParser parser = new GnuParser();
		AtlConfigurationLoader atlConfigurationLoader = new AtlConfigurationLoader();

		try {
			CommandLine e = parser.parse(options, args);
			if (e.hasOption("q")) {
				ATLLogger.getLogger().setLevel(Level.OFF);
			}

			String configurationFile = e.getOptionValue("c");
			AtlConfiguration atlConfiguration = atlConfigurationLoader.loadConfiguration(configurationFile);
			
			URI transformationUri = URI.createURI(atlConfiguration.getTransformationLocation());
			AtlExecEnvBuilder builder = AtlExecEnvBuilder.buildExecEnv(transformationUri);
			
			for (AtlModelDefinition atlModelDefinition : atlConfiguration.getSourceMetaModelDefinitions()) {
				URI metamodelUri = URI.createURI(atlModelDefinition.getModelLocation());
				builder.loadMetamodel(
						atlModelDefinition.getModelName(),
						metamodelUri,
						TransformationDirectionCodes.INPUT);
			}
			
			for (AtlModelDefinition atlModelDefinition : atlConfiguration.getTargetMetaModelDefinitions()) {
				URI metamodelUri = URI.createURI(atlModelDefinition.getModelLocation());
				builder.loadMetamodel(
						atlModelDefinition.getModelName(),
						metamodelUri,
						TransformationDirectionCodes.OUTPUT);
			}
			
			for (AtlModelDefinition atlModelDefinition : atlConfiguration.getInputModelDefinitions()) {
				URI modelUri = URI.createURI(atlModelDefinition.getModelLocation());
				builder.loadModel(
						atlModelDefinition.getModelName(),
						modelUri,
						TransformationDirectionCodes.INPUT);
			}
			
			for (AtlModelDefinition atlModelDefinition : atlConfiguration.getOutputModelDefinitions()) {
				URI modelUri = URI.createURI(atlModelDefinition.getModelLocation());
				builder.loadModel(
						atlModelDefinition.getModelName(),
						modelUri,
						TransformationDirectionCodes.OUTPUT);
			}

			TimingData td = new TimingData();
			ExecEnv execEnv = builder.getExecEnv();
			td.finishLoading();
			execEnv.run(td);
			td.finish();
			ATLLogger.info(td.toString());
			
			Iterator<Model> outputModels = execEnv.getOutputModels().values().iterator();

			Model model;
			while (outputModels.hasNext()) {
				model = (Model) outputModels.next();
				Resource modelResource = model.getResource();
				modelResource.save(Collections.emptyMap());
			}

			Iterator<Model> inoutModels = execEnv.getInoutModels().values().iterator();

			while (inoutModels.hasNext()) {
				model = (Model) inoutModels.next();
				model.getResource().save(Collections.emptyMap());
			}
		} catch (ParseException arg26) {
			System.err.println(arg26.getLocalizedMessage());
			HelpFormatter formatter = new HelpFormatter();
			formatter.setOptionComparator(new OptionComarator<>());

			try {
				formatter.setWidth(Math.max(TerminalFactory.get().getWidth(), 80));
			} catch (Throwable arg25) {
				;
			}

			formatter.printHelp("java -jar <this-file.jar>", options, true);
		}

	}

	private static void configureOptions(Options options) {
		Option transformationOpt = OptionBuilder.create("c");
		transformationOpt.setLongOpt("conf");
		transformationOpt.setArgName("path_to_configuration.json");
		transformationOpt.setDescription("ATL transformation configuration");
		transformationOpt.setArgs(1);
		transformationOpt.setRequired(false);
		Option quietOption = OptionBuilder.create("q");
		quietOption.setLongOpt("quiet");
		quietOption.setDescription(
				"Do not print any information about the transformation execution on the standard output (optional, defaults to false)");
		quietOption.setArgs(0);
		options.addOption(transformationOpt);
		options.addOption(quietOption);
	}
}