package pt.pikes.atl.runner;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.m2m.atl.emftvm.EmftvmFactory;
import org.eclipse.m2m.atl.emftvm.ExecEnv;
import org.eclipse.m2m.atl.emftvm.Metamodel;
import org.eclipse.m2m.atl.emftvm.Model;
import org.eclipse.m2m.atl.emftvm.ModelDeclaration;
import org.eclipse.m2m.atl.emftvm.Module;
import org.eclipse.m2m.atl.emftvm.util.DefaultModuleResolver;

public class AtlExecEnvBuilder {
	
	private ExecEnv execEnv;
	private Module module;
	private ResourceSet resourceSet;
	private URI transformationUri;
	
	private AtlExecEnvBuilder(URI transformationUri) {
		// Require that some resources must be already initialized.
		ResourcesBootstraper.bootstrapResources();
		
		this.execEnv = EmftvmFactory.eINSTANCE.createExecEnv();
		this.resourceSet = new ResourceSetImpl();
		this.transformationUri = transformationUri;
		
		// Initialization logic
		this.setTransformation();
	}
	
	private void setTransformation() {
		
		if (module == null) {
			Resource transformationResource = resourceSet.getResource(this.transformationUri, true);
			this.module = (Module) transformationResource.getContents().get(0);
		}
	}
	
	private Resource getOrCreateResourceIfNotExists(URI uri) {
		File modelFile = new File(uri.path());
		if (modelFile.exists()) {
			return this.resourceSet.getResource(uri, true);
		} else {
			return this.resourceSet.createResource(uri);
		}
	}
	
	private List<ModelDeclaration> getModelDeclarations(TransformationDirectionCodes transformationDirectionCode) {
		switch (transformationDirectionCode) {
			case INPUT:
				return this.module.getInputModels();
			case INPUT_OUTPUT:
				return this.module.getInoutModels();
			case OUTPUT:
				return this.module.getOutputModels();
			default:
				throw new IllegalArgumentException("Unknown transformation direction: '" + transformationDirectionCode.name() + "'");
				
		}
	}
	
	private void registerMetamodel(String metamodelName, Metamodel metamodel) {
		this.execEnv.registerMetaModel(metamodelName, metamodel);
		
		EObject eObject = (EObject) metamodel.getResource().getContents().get(0);
		if (eObject instanceof EPackage) {
			EPackage p = (EPackage) eObject;
			this.resourceSet.getPackageRegistry().put(p.getNsURI(), p);
		}

	}
	
	private void registerModel(String modelName, Model model, TransformationDirectionCodes transformationDirectionCode) {
		switch (transformationDirectionCode) {
			case INPUT:
				this.execEnv.registerInputModel(modelName, model);
				break;
			case INPUT_OUTPUT:
				this.execEnv.registerInOutModel(modelName, model);
				break;
			case OUTPUT:
				this.execEnv.registerOutputModel(modelName, model);
				break;
			default:
				throw new IllegalArgumentException("Unknown transformation direction: '" + transformationDirectionCode.name() + "'");
		}
	}
	
	public void loadMetamodel(String metamodelName, URI metamodelUri, TransformationDirectionCodes transformationDirectionCode) {
		if (!this.getModelDeclarations(transformationDirectionCode)
				.stream()
				.anyMatch(md -> md.getMetaModelName().equalsIgnoreCase(metamodelName))) {
			throw new IllegalArgumentException("Specified metamodel name '" + metamodelName + "' was not found on transformation.");
		}
		
		Metamodel metamodel = EmftvmFactory.eINSTANCE.createMetamodel();
		metamodel.setResource(resourceSet.getResource(metamodelUri, true));
		
		this.registerMetamodel(metamodelName, metamodel);
	}
	
	public void loadModel(String modelName, URI modelUri, TransformationDirectionCodes transformationDirectionCode) {
		if (!this.getModelDeclarations(transformationDirectionCode)
				.stream()
				.anyMatch(md -> md.getModelName().equalsIgnoreCase(modelName))) {
			throw new IllegalArgumentException("Specified model name '" + modelName + "' was not found on transformation.");
		}
		
		Model model = EmftvmFactory.eINSTANCE.createModel();
		model.setResource(this.getOrCreateResourceIfNotExists(modelUri));
		model.setAllowInterModelReferences(true);
		this.registerModel(modelName, model, transformationDirectionCode);
	}
	
	public ExecEnv getExecEnv() {
		String moduleName = module.getName();
		String transformationFilePath = this.transformationUri.path();
		
		Path transformationPath = Paths.get(transformationFilePath, new String[0]);
		
		// Build transformation working path.
		StringBuilder stringBuilder = new StringBuilder();
		if (transformationPath.getParent() != null) {
			stringBuilder.append(transformationPath.getParent().toString());
		} else {
			stringBuilder.append(".");
		}
		
		stringBuilder.append(File.separator);
		String workingPath = stringBuilder.toString();
		
		DefaultModuleResolver mr = new DefaultModuleResolver(workingPath, this.resourceSet);
		this.execEnv.loadModule(mr, moduleName);
		
		return this.execEnv;
	}
	
	public static AtlExecEnvBuilder buildExecEnv(URI transformationUri) {
		return new AtlExecEnvBuilder(transformationUri);
	}
}
