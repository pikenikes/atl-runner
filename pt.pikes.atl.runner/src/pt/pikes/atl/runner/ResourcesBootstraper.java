package pt.pikes.atl.runner;

import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.m2m.atl.emftvm.impl.resource.EMFTVMResourceFactoryImpl;
import org.eclipse.uml2.uml.UMLPackage;

public class ResourcesBootstraper {
	private static boolean areResourcesInitialized;
	
	public static void bootstrapResources() {
		if (!areResourcesInitialized) {
			Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
			Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
			Registry.INSTANCE.getExtensionToFactoryMap().put("emftvm", new EMFTVMResourceFactoryImpl());
			
			// Adds UML 2.0 support
			Registry.INSTANCE.getExtensionToFactoryMap().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
		}
	}
}
