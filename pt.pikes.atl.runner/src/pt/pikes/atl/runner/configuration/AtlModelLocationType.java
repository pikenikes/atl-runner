package pt.pikes.atl.runner.configuration;

public enum AtlModelLocationType {
	FILE,
	URI
}
