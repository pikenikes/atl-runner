package pt.pikes.atl.runner.configuration;

import java.util.ArrayList;
import java.util.List;

public class AtlConfiguration {
	
	private String transformationLocation;
	
	private List<AtlModelDefinition> sourceMetaModelDefinitions;
	
	private List<AtlModelDefinition> targetMetaModelDefinitions;
	
	private List<AtlModelDefinition> inputModelDefinitions;
	
	private List<AtlModelDefinition> outputModelDefinitions;
	
	public AtlConfiguration(String transformationLocation) {
		this.transformationLocation = transformationLocation;
		this.inputModelDefinitions = new ArrayList<>();
		this.outputModelDefinitions = new ArrayList<>();
		this.sourceMetaModelDefinitions = new ArrayList<>();
		this.targetMetaModelDefinitions = new ArrayList<>();
	}
	
	private AtlModelDefinition createModelDefinition(String alias, String location) {
		AtlModelLocationType modelLocationType;
		
		if (location.startsWith("http://")) {
			modelLocationType = AtlModelLocationType.URI;
		} else {
			modelLocationType = AtlModelLocationType.FILE;
		}
		
		return new AtlModelDefinition(alias, location, modelLocationType);
	}
	
	public void addSourceMetaModel(String alias, String location) {
		AtlModelDefinition modelDefinition = this.createModelDefinition(alias, location);
		
		this.sourceMetaModelDefinitions.add(modelDefinition);
	}
	
	public void addTargetMetaModel(String alias, String location) {
		AtlModelDefinition modelDefinition = this.createModelDefinition(alias, location);
		
		this.targetMetaModelDefinitions.add(modelDefinition);
	}
	
	public void addInputModel(String alias, String location) {
		AtlModelDefinition modelDefinition = this.createModelDefinition(alias, location);
		
		this.inputModelDefinitions.add(modelDefinition);
	}
	
	public void addOutputModel(String alias, String location) {
		AtlModelDefinition modelDefinition = this.createModelDefinition(alias, location);
		
		this.outputModelDefinitions.add(modelDefinition);
	}
	
	public String getTransformationLocation() {
		return this.transformationLocation;
	}
	
	public Iterable<AtlModelDefinition> getSourceMetaModelDefinitions() {
		return this.sourceMetaModelDefinitions;
	}
	
	public Iterable<AtlModelDefinition> getTargetMetaModelDefinitions() {
		return this.targetMetaModelDefinitions;
	}
	
	public Iterable<AtlModelDefinition> getInputModelDefinitions() {
		return this.inputModelDefinitions;
	}
	
	public Iterable<AtlModelDefinition> getOutputModelDefinitions() {
		return this.outputModelDefinitions;
	}
}
