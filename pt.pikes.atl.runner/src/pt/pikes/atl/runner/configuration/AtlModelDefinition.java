package pt.pikes.atl.runner.configuration;

public class AtlModelDefinition {
	private String modelName;
	
	private String modelLocation;
	
	private AtlModelLocationType modelLocationType;
	
	public AtlModelDefinition(
			String modelName,
			String modelLocation,
			AtlModelLocationType modelLocationType) {
		this.modelName = modelName;
		this.modelLocation = modelLocation;
		this.modelLocationType = modelLocationType;
	}
	
	public String getModelName() {
		return this.modelName;
	}
	
	public String getModelLocation() {
		return this.modelLocation;
	}
	
	public AtlModelLocationType getModelLocationType() {
		return this.modelLocationType;
	}
}
