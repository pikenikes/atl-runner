package pt.pikes.atl.runner.configuration;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class AtlConfigurationLoader {
	private JSONParser jsonParser;
	
	public AtlConfigurationLoader() {
		this.jsonParser = new JSONParser();
	}
	
	public AtlConfiguration loadConfiguration(String filePath) {
		AtlConfiguration atlConfiguration = null;
		
		try {
			File configurationFile = new File(filePath);
			JSONObject configurationRoot = (JSONObject)this.jsonParser.parse(new FileReader(configurationFile.getAbsoluteFile()));
			
			String transformationLocation = (String)configurationRoot.get("TransformationLocation");
			
			atlConfiguration = new AtlConfiguration(transformationLocation);
			
			JSONArray sourceMetaModelsArray = (JSONArray)configurationRoot.get("SourceMetaModels");
			
			for (Object sourceMetaModel : sourceMetaModelsArray) {
				JSONObject sourceMetaModelJson = (JSONObject)sourceMetaModel;
				
				atlConfiguration.addSourceMetaModel(
						(String)sourceMetaModelJson.get("Name"),
						(String)sourceMetaModelJson.get("Location"));
			}
			
			JSONArray targetMetaModelsArray = (JSONArray)configurationRoot.get("TargetMetaModels");
			
			for (Object targetMetaModel : targetMetaModelsArray) {
				JSONObject targetMetaModelJson = (JSONObject)targetMetaModel;
				
				atlConfiguration.addTargetMetaModel(
						(String)targetMetaModelJson.get("Name"),
						(String)targetMetaModelJson.get("Location"));
			}
			
			JSONArray inputModelsArray = (JSONArray)configurationRoot.get("InputModels");
			
			for (Object inputModel : inputModelsArray) {
				JSONObject inputModelJson = (JSONObject)inputModel;
				
				atlConfiguration.addInputModel(
						(String)inputModelJson.get("Name"),
						(String)inputModelJson.get("Location"));
			}
			
			JSONArray outputModelsArray = (JSONArray)configurationRoot.get("OutputModels");
			
			for (Object outputModel : outputModelsArray) {
				JSONObject outputModelJson = (JSONObject)outputModel;
				
				atlConfiguration.addOutputModel(
						(String)outputModelJson.get("Name"),
						(String)outputModelJson.get("Location"));
			}
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return atlConfiguration;
	}
}
