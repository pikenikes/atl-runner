package pt.pikes.atl.runner;

public enum TransformationDirectionCodes {
	INPUT,
	OUTPUT,
	INPUT_OUTPUT
}
