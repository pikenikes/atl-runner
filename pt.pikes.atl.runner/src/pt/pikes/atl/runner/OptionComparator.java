package pt.pikes.atl.runner;

import java.util.Comparator;
import org.apache.commons.cli.Option;

class OptionComarator<T extends Option> implements Comparator<T> {
	private static final String OPTS_ORDER = "cq";

	public OptionComarator() {
	}

	public int compare(T o1, T o2) {
		return OPTS_ORDER.indexOf(o1.getOpt()) - OPTS_ORDER.indexOf(o2.getOpt());
	}
}
