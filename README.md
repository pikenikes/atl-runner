# ATL Runner

A Java tool to run ATL/EMFVM transformations.

## Features

* Configurable using JSON configuration file.
* Multiple input/output models.